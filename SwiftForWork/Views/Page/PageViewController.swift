//
//  PageViewController.swift
//  SwiftForWork
//
//  Created by Admin on 9/25/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {
    private lazy var pages: [UIViewController] = {
        return [
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pageOne"),
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pageTwo")
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        
        guard let firstPage = pages.first else { return }
        
        setViewControllers([firstPage], direction: .forward, animated: true, completion: nil)
    }

}

extension PageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pages.count else { return pages[0] }
        
        return pages[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else { return pages[pages.count - 1] }
        
        return pages[previousIndex]
    }
}
