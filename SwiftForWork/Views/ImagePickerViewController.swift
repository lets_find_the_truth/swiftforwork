//
//  ImagePickerViewController.swift
//  SwiftForWork
//
//  Created by Admin on 3/20/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class ImagePickerViewController: UIViewController {
    private let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary //can be camera
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)! //for video availability
    }
    
    @IBAction func openImageButtonDidTap(_ sender: UIButton) {
        imagePicker.modalPresentationStyle = .popover //for ipad
        imagePicker.popoverPresentationController?.delegate = self
        imagePicker.popoverPresentationController?.sourceView = view
        self.present(imagePicker, animated: true, completion: nil)
    }

}

extension ImagePickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
       /* "UIImagePickerControllerImageURL" file:///Users/admin/Library/Developer/CoreSimulator/Devices/F48AF1ED-1E1E-4558-BF0F-6E2B47101363/data/Containers/Data/Application/C03762F0-A637-49D2-A441-D50E4368E706/tmp/07C0EC8B-7831-49C3-A332-529C0DF991B5.jpeg
        "UIImagePickerControllerMediaType" public.image or public.movie
       "UIImagePickerControllerReferenceURL" assets-library://asset/asset.JPG?id=B84E8479-475C-4727-A4A4-B77AA9980897&ext=JPG
        - key : "UIImagePickerControllerOriginalImage" UIImage */
        //get imageData or videoData
        /*
        let imageURL = info[UIImagePickerControllerImageURL] as! URL
     
        do {
            let imageData = try Data(contentsOf: imageURL)
        } catch let error {} */
        //or UIImageJPEGRepresentation(image, 1.0)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension ImagePickerViewController: UIPopoverPresentationControllerDelegate {
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        view.alpha = 1.0
    }
}
