//
//  AutoCellSizing.swift
//  SwiftForWork
//
//  Created by Kyryl Nevedrov on 12/08/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class ArticlesFilterCollectionViewCell: UICollectionViewCell {
    // MARK: - @IBOutlets
    @IBOutlet weak var collectionView: UICollectionViewCell!
    
    // MARK: - Properties
    static let reuseIdentifier = "ArticlesFilterCollectionViewCell"
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame = layoutAttributes.frame
        frame.size.width = ceil(size.width)
        layoutAttributes.frame = frame
        return layoutAttributes
    }
}
