//
//  WebViewController.swift
//  SwiftForWork
//
//  Created by Admin on 9/26/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "https://www.google.com")!
        let request = URLRequest(url: url)
        webView.load(request)
    }
    
    @IBAction func openInSafariButtonDidTap(_ sender: UIButton) {
        if let url = URL(string: "https://www.google.com") {
            UIApplication.shared.open(url, options: [:])
        }
    }
}
