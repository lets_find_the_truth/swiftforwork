//
//  MainViewController.swift
//  SwiftForWork
//
//  Created by Admin on 10/1/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func menuButtonDidTap(_ sender: UIBarButtonItem) {
        NotificationCenter.default.post(name: NSNotification.Name.init("showMenu"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
