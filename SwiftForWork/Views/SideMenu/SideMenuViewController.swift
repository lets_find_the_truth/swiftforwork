//
//  SideMenuViewController.swift
//  SwiftForWork
//
//  Created by Admin on 10/1/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    @IBOutlet weak var leadingSideMenuConstraint: NSLayoutConstraint!
    private var isMenuOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init("showMenu"), object: nil, queue: .main) { (notification) in
            self.toggleMenu()
        }
    }
    
    func toggleMenu() {
        leadingSideMenuConstraint.constant = isMenuOpen ? -240 : 0
        isMenuOpen = !isMenuOpen
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
