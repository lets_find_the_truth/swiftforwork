//
//  TableViewController.swift
//  SwiftForWork
//
//  Created by Admin on 3/15/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

/*For moving
tableView.isEditing = true
func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool
func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
*/

/*For swipe action
 
 a) from iOS11
 func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
 let editAction = UIContextualAction(style: .normal, title: "Edit") { (action, view, completion) in
 view.backgroundColor = .black //color after click
 }
 deleteAction.backgroundColor = .red //color
 return UISwipeActionsConfiguration(actions: [deleteAction])
 }
 
 b) under iOS11
 func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
 let deleteAction = UITableViewRowAction(style: .destructive, title: "delete") { (action, indexPath) in
 self.tableView(tableView, commit: .delete, forRowAt: indexPath)
 }
 return [deleteAction]
 }
*/

/*For auto height
 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
 return UITableViewAutomaticDimension
 }
 func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return myEstimatedRowSize
 }
*/

