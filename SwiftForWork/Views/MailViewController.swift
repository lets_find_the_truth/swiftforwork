//
//  MailViewController.swift
//  SwiftForWork
//
//  Created by Admin on 3/5/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import MessageUI

class MailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        sendEmail()
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mailController = MFMailComposeViewController()
            mailController.mailComposeDelegate = self
            mailController.setSubject("Subject")
            mailController.setToRecipients(["DarkLir@gmail.com"])
            mailController.setMessageBody(
                """
                Hi, I am Nevedrov Kyryl. I am iOS Developer.
                Do you want to hire me?
                """
                , isHTML: true)
            //add image atachment
            let image = UIImage(named: "Wisdom")!
            let imageData = image.jpegData(compressionQuality: 1)!
            mailController.addAttachmentData(imageData, mimeType: "image/jpeg", fileName: "image.jpeg")
            self.present(mailController, animated: true, completion: nil)
            
        } else {
            //can't send emails(simulator as example)
        }
    }
}

extension MailViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
