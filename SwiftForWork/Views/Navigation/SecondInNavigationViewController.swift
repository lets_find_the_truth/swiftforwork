//
//  SecondInNavigationViewController.swift
//  SwiftForWork
//
//  Created by Admin on 9/26/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class SecondInNavigationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
      self.navigationController?.navigationBar.tintColor = UIColor.red // back button will be red in all VC
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
