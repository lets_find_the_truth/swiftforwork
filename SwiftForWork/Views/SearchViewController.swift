//
//  SearchViewController.swift
//  SwiftForWork
//
//  Created by Admin on 3/19/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var items = ["Label","Button1-初级","Button1-高级","Button2-初级","Button2-高级","Switch"]
    var filteredItems: [String] = []
    enum SearchScope: String {
        case Button
        case Other
        case All
    }
    var scopeItems = [SearchScope.Button.rawValue, SearchScope.Other.rawValue, SearchScope.All.rawValue]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filteredItems = items
        searchBar.showsScopeBar = true
        searchBar.scopeButtonTitles = scopeItems
        searchBar.showsCancelButton = true
        searchBar.showsBookmarkButton = true
        //searchBar.showsSearchResultsButton = true
    }

}

extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            filteredItems = items
            return
        }
        
        filteredItems = items.filter({ (text) -> Bool in
            text.lowercased().contains(searchText.lowercased())
        })
        itemsTableView.reloadData()
    }// called when text changes (including clear)
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("shouldChangeTextIn")
        return true
    }// called before text changes
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int){
        print("selectedScopeButtonIndexDidChange")
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        print("searchBarShouldBeginEditing")
        return true
    }// return NO to not become first responder
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
    }// called when text starts editing
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        print("")
        return true
    }// return NO to not resign first responder
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidEndEditing")
    }// called when text ends editing
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        print("searchBarSearchButtonClicked")
    } // called when keyboard search button pressed
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar){
        print("searchBarBookmarkButtonClicked")
    } // called when bookmark button pressed
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        print("searchBarCancelButtonClicked")
    } // called when cancel button pressed
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar){
        print("searchBarResultsListButtonClicked")
    } // called when search results button pressed
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath)
        cell.textLabel?.text = filteredItems[indexPath.row]
        return cell
    }
}
