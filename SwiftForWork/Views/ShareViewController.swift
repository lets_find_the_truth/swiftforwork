//
//  ShareViewController.swift
//  SwiftForWork
//
//  Created by Admin on 3/14/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func shareTextAndImage(text: String, image: UIImage) {
        let activityController = UIActivityViewController(activityItems: [text, image], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        activityController.excludedActivityTypes = [.airDrop, .postToFacebook]
        present(activityController, animated: true, completion: nil)
    }
    

}
