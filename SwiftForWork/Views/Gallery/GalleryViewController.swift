//
//  GalleryViewController.swift
//  SwiftForWork
//
//  Created by Admin on 9/20/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import Photos

class GalleryViewController: UIViewController {
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    
    private var photoAssets: PHFetchResult<PHAsset>? {
        didSet {
            galleryCollectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchAlbums()
        let screenSize = UIScreen.main.bounds
        let space: CGFloat = 5.0
        let itemsPerLine: CGFloat = 3.0
        let insectLeftRight: CGFloat = 10
        let width = (screenSize.size.width - space - insectLeftRight * 2) / itemsPerLine - space
        let height = width
        flowLayout.itemSize = CGSize(width: width, height: height)
        flowLayout.minimumLineSpacing = 10 // minimum spaceing between lines (vertical)
        flowLayout.minimumInteritemSpacing = 10 //minimum spacing betweet items in same row
        flowLayout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10) //space between collection view and view
    
    }
    
    private func fetchAlbums() {
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "localizedTitle", ascending: true)]
        let smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .smartAlbumUserLibrary, options: fetchOptions)
        
        fetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.image.rawValue)
        let assetsFetchOptions = PHFetchOptions()
        assetsFetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        assetsFetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.image.rawValue)
        [smartAlbums].forEach {
            $0.enumerateObjects { collection, index, stop in
                self.photoAssets = PHAsset.fetchAssets(in: collection, options: assetsFetchOptions)
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}

extension GalleryViewController: UICollectionViewDelegate {
    
}

extension GalleryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let photoAssets = photoAssets else {
            return 0
        }
        return photoAssets.count
    }
    
    //footer header create in Storyboard
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "collectionHeader", for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellModel = GalleryCollectionViewCellModel(image: getAssetThumbnail(asset: photoAssets![indexPath.row], thubnailSize: CGSize(width: 200, height: 200))!)
        return collectionView.dequeueReusableCell(withModel: cellModel, for: indexPath)
    }
    
    private func getAssetThumbnail(asset: PHAsset, thubnailSize: CGSize) -> UIImage? {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.isSynchronous = true
        option.deliveryMode = .highQualityFormat
        var thumbnail: UIImage?
        //use targetSize
        manager.requestImage(for: asset, targetSize: thubnailSize, contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result
        })
        return thumbnail
    }
}
