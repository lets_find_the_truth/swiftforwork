//
//  GalleryCollectionViewCell.swift
//  SwiftForWork
//
//  Created by Admin on 9/20/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
