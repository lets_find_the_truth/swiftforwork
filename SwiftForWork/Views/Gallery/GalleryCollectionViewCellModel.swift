//
//  GalleryCollectionViewCellModel.swift
//  SwiftForWork
//
//  Created by Admin on 9/20/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

struct GalleryCollectionViewCellModel {
    var image: UIImage
}

extension GalleryCollectionViewCellModel: CellViewModel {
    func setup(cell: GalleryCollectionViewCell) {
        cell.imageView.image = image
    }
}
