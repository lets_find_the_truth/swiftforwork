//
//  LocalNotifications.swift
//  SwiftForWork
//
//  Created by Admin on 3/14/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import UserNotifications

// UNUserNotificationCenter.current().delegate = self in AppDelegate and and Delegate methods 
class LocalNotifications {

    func show() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            self.showNotification()
        }
    }
    
    func showNotification() {
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = "Title"
        content.badge = 1
        content.subtitle = "Subtitle my"
        content.body = "Body my"
        content.sound = UNNotificationSound.default
        
        let action = UNNotificationAction(identifier: "identifier action", title: "DELETE", options: [.destructive])
        let textAction = UNTextInputNotificationAction(identifier: "textInputAction", title: "Name", options: [], textInputButtonTitle: "Go", textInputPlaceholder: "Name")
        let category = UNNotificationCategory(identifier: "category", actions: [action, textAction], intentIdentifiers: [], options: [.customDismissAction])
        content.categoryIdentifier = "category"
        
        center.setNotificationCategories([category])
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 4, repeats: false)
        let request = UNNotificationRequest(identifier: "request", content: content, trigger: trigger)
        center.add(request) { (error) in
            
        }
    }
    
    
}
