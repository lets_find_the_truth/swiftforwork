//
//  Settings.swift
//  SwiftForWork
//
//  Created by Admin on 3/14/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

/*
 Add settings bundle(new file)
 
 */

class Settings {
    static func getValues() {
        UserDefaults.standard.object(forKey: "game_preference") // get value from setting
    }
    
    static func change() {
        let isSound = UserDefaults.standard.bool(forKey: "enabled_preference")
        UserDefaults.standard.set(!isSound, forKey: "enabled_preference") //Change setting
    }
    
}
