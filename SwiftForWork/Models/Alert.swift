//
//  Alert.swift
//  SwiftForWork
//
//  Created by Admin on 9/24/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class Alert {
    private var alert: UIAlertController!
    private var viewController: UIViewController!
    
    init (title: String?, message: String?, style: UIAlertController.Style, viewController: UIViewController) {
        self.alert = UIAlertController(title: title, message: message, preferredStyle: style)
        self.viewController = viewController
    }
    
    func addAction(title: String, style: UIAlertAction.Style, handler: ((UIAlertAction) -> Void)?) -> Alert {
        let action = UIAlertAction(title: title, style: style, handler: handler)
        self.alert.addAction(action)
        return self
    }
    
    func show() {
        viewController.present(alert, animated: true, completion: nil)
    }
    
}
