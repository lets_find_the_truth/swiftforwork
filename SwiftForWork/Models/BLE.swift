//
//  BLE.swift
//  SwiftForWork
//
//  Created by Admin on 9/17/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import CoreBluetooth

class BLEManager: NSObject {
    static let shared = BLEManager()
    
    fileprivate var peripheral: CBPeripheral!
    fileprivate var manager: CBCentralManager!
    
    
    fileprivate var rxCharacteristic: CBCharacteristic!
    fileprivate var txCharacteristic: CBCharacteristic!
    
    private override init () {
        super.init()
    }
    
    // MARK: Start BLE manager
    func startManager() {
        self.manager = CBCentralManager(delegate: self, queue: nil)
    }
    
    // MARK: Start scan for BLE devices
    func startScan() {
        manager.scanForPeripherals(withServices: nil, options: nil)
    }
    
    // MARK: Stop scan for BLE devices
    func stopScan() {
        self.manager.stopScan()
    }
    
    // MARK: Get notifcation from characteristic
    func getNotificationData(characteristic: CBCharacteristic) {
        let notificationData: Data = characteristic.value!
    }
    
    // MARK: BLE commands
    func sendShutDownCommand() {
        guard (peripheral != nil && txCharacteristic != nil) else {
            return
        }
        
        let shutDownCommandData = NSMutableData()
        var firstByte = 0x01
        shutDownCommandData.append(&firstByte, length: 1)
        self.peripheral.writeValue(shutDownCommandData as Data, for: self.txCharacteristic, type: .withoutResponse)
    }
}

// MARK: CBCentralManagerDelegate methods
extension BLEManager: CBCentralManagerDelegate {
    
    // MARK: Invoked whenever the central manager's state has been updated.
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            startScan()
        case .poweredOff:
            break
        case .resetting:
            break
        case .unauthorized:
            break
        case .unknown:
            break
        case .unsupported:
            break
        }
    }
    
    // MARK: This method is invoked while scanning, upon the discovery of peripheral.
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        guard peripheral.name != nil else {
            return
        }
        
        self.stopScan()
        self.peripheral = peripheral
        self.peripheral.delegate = self
        self.manager.connect(self.peripheral, options: nil)
    }
    
    // MARK: This method is invoked when a connection initiated by connectPeripheral has succeeded.
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.discoverServices(nil)
    }
    
    // MARK: This method is invoked when a connection initiated by connectPeripheral has failed to complete.
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
    }
    
    // MARK: This method is invoked upon the disconnection of a peripheral that was connected by connectPeripheral.
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        if rxCharacteristic != nil {
            self.peripheral.setNotifyValue(false, for: rxCharacteristic)
            
        }
        self.peripheral.delegate = nil
        self.peripheral = nil
        self.txCharacteristic = nil
        self.rxCharacteristic = nil
        startScan()
    }
    
}

// MARK: CBPeripheralDelegate methods
extension BLEManager: CBPeripheralDelegate {
    
    // MARK: This method returns the result of a discoverServices call.
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard error == nil else {
            return
        }
        
        for service in peripheral.services! {
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    // MARK: This method returns the result of a discoverCharacteristics call.
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard error == nil else {
            return
        }
        
        for characteristic in service.characteristics! {
            if characteristic.uuid == CBUUID(string: "0x2B10") {
                self.peripheral.setNotifyValue(true, for: characteristic)
                self.rxCharacteristic = characteristic
            }
        }
    }
    
    // MARK: This method returns the result of a writeValue call, when the CBCharacteristicWriteWithResponse type is used.
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        guard error == nil else {
            return
        }
    }
    
    // MARK: This method is invoked after a readValueForCharacteristic call, or upon receipt of a notification/indication.
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        guard error == nil else {
            return
        }
        getNotificationData(characteristic: characteristic)
    }
    
    // MARK: This method returns the result of a setNotifyValue:forCharacteristic call.
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        if characteristic.uuid == CBUUID(string: "0x2B10") {
            guard error == nil else {
                return
            }
        }
    }
}

extension Data {
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}

