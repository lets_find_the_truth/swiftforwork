//
//  Dates.swift
//  SwiftForWork
//
//  Created by Admin on 9/17/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class Dates {
    private let dateFormatter = DateFormatter()
    
    init(dateFormat: String) {
        //change to dateFormatter.dateFormat = dateFormat
        dateFormatter.dateFormat = "MM/dd/yyyy"
//        Monday, Sep 17, 2018    EEEE, MMM d, yyyy
//        09/17/2018   MM/dd/yyyy
//        09-17-2018 07:32  MM-dd-yyyy HH:mm
//        Sep 17, 7:32    AM MMM d, h:mm a
//        September 2018   MMMM yyyy
//        Sep 17, 2018   MMM d, yyyy
//        Mon, 17 Sep 2018 07:32:32 +0000   E, d MMM yyyy HH:mm:ss Z
//        2018-09-17T07:32:32+0000    yyyy-MM-dd'T'HH:mm:ssZ
//        17.09.18    dd.MM.yy
    }
    
    func getDateFrom(string: String) -> Date? {
        return dateFormatter.date(from: string)
    }
    
    func getStringFrom(date: Date) -> String {
        return dateFormatter.string(from: date)
    }
    
    func otherMethods(date: Date, otherDate: Date) {
        Calendar.current.isDateInToday(date)//and isTomorrow, inWeek, isYesterday
        Calendar.current.compare(date, to: otherDate, toGranularity: .day).rawValue //1 -1 0
        Calendar.current.component(.day, from: date)
        Calendar.current.date(byAdding: .day, value: 1, to: date)!
        Calendar.current.date(bySetting: .day, value: 25, of: date)!
    }
}
