//
//  NetworkManager.swift
//  SwiftForWork
//
//  Created by Admin on 3/15/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire

protocol CustomError: Error {
    var localizedDescription: String { get }
    var localizedTitle: String { get }
    var code: Int { get }
}

class NetworkError: CustomError {
    var localizedDescription: String
    var localizedTitle: String
    var code: Int
    
    init(localizedDescription: String, localizedTitle: String, code: Int) {
        self.localizedDescription = localizedDescription
        self.localizedTitle = localizedTitle
        self.code = code
    }
}

enum NetworkErrorType: Error {
    case JSONParseError
}

class NetworkManager {
    static let shared = NetworkManager()
    
    private var manager: Alamofire.SessionManager!
    
    private init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 5
        self.manager = Alamofire.SessionManager(configuration: configuration)
    }
    
    
    func request<T: RequestHandler>(model: RequestModel, handler: T ,completion: @escaping (T, NetworkError?) -> Void) {

        // JSON parsing function
        func parseJSON(_ response: DataResponse<Any>) {
            switch response.result {
            case .success:
                do {
                    try handler.parseJSON(JSON: response.result.value!)
                    completion(handler, nil)
                } catch {
                    completion(handler, NetworkError(localizedDescription: "Parse error", localizedTitle: "", code: 0))
                }
                
            case .failure:
                if let error = response.result.error as? AFError, error.isResponseSerializationError {
                    completion(handler, nil)
                } else {
                    completion(handler, NetworkError(localizedDescription: "Status code not \(model.successCodes)", localizedTitle: "", code: response.response!.statusCode))
                }
            }
        }
        
        switch model.method {
        case .get:
            let url = URL(string: model.URL)
            var urlRequest = URLRequest(url: url!)
            urlRequest.httpMethod = model.method.rawValue
            for (key, value) in model.headers ?? [:] {
                urlRequest.addValue(value, forHTTPHeaderField: key)
            }
            
            self.manager.request(urlRequest).validate(statusCode: model.successCodes).responseJSON(completionHandler: { (response) in
                parseJSON(response)
            })
        case .post, .put, .delete:
            self.manager.request(model.URL, method: model.method, parameters: model.parameters, encoding: JSONEncoding.default, headers: model.headers).validate(statusCode: model.successCodes).responseJSON { (response) in
                parseJSON(response)
            }
        default:
            break
        }
    }
    
    func upload<T: RequestHandler>(model: UploadMultipartRequestModel, handler: T, uploadProgress: @escaping (Double) -> Void, completion: @escaping (T, NetworkError?) -> Void) {
        manager.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(model.fileURL, withName: model.fileName)
        }, to: model.URL, method: model.method, headers: model.headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    uploadProgress(progress.fractionCompleted)
                })
                upload.responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        do {
                            try handler.parseJSON(JSON: value)
                            completion(handler, nil)
                        } catch {
                            completion(handler, NetworkError(localizedDescription: "Parse error", localizedTitle: "", code: 0))
                        }
                    case .failure(let error):
                        completion(handler, NetworkError(localizedDescription: error.localizedDescription, localizedTitle: "", code: 0))
                    }
                }
                
            case .failure(let encodingError):
                completion(handler, NetworkError(localizedDescription: encodingError.localizedDescription, localizedTitle: "", code: 0))
            }
        }

    }
}

