//
//  RequestHandler.swift
//  SwiftForWork
//
//  Created by Admin on 3/15/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Foundation

protocol RequestHandler {
    func parseJSON(JSON: Any) throws
}

extension RequestHandler {
    func parseJSON(JSON: Any) throws {}
}
