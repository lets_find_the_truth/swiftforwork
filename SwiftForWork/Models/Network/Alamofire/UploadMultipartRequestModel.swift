//
//  UploadMultipartRequestModel.swift
//  SwiftForWork
//
//  Created by Admin on 3/15/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

protocol UploadMultipartRequestModel: RequestModel {
    var fileURL: URL! { get set }
    var fileName: String { get set }
}
