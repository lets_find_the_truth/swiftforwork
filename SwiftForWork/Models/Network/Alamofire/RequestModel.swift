//
//  RequestModel.swift
//  SwiftForWork
//
//  Created by Admin on 3/15/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire

protocol RequestModel {
    var URL: String { get  set }
    var method: HTTPMethod { get set }
    var parameters: [String: Any]? { get set }
    var headers: HTTPHeaders? { get set }
    var successCodes: [Int] { get set }
}

