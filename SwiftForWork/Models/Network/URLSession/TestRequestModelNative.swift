//
//  TestRequestModelNative.swift
//  SwiftForWork
//
//  Created by Admin on 9/18/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class TextRequstModelNative: RequestModelNative {
    var url: URL
    var method: HTTPSessionMethod
    var headers: [String : String] = [:]
    var parameters: [String : Any]?
    var successCodes: [Int]
    
    init() {
        url = URL(string:"https://jsonplaceholder.typicode.com/posts")!
        method = .post
        successCodes = [201]
        parameters = [
            "title": "foo",
            "body": "bar2",
            "userId": 1
        ]
        headers = [
            "Content-type": "application/json; charset=UTF-8"
        ]
    }
}

class TestRequestHandlerNative: RequestHandlerNative {
    var body: String
    var id: Int
 
    enum CodingKeys: CodingKey {
        case body
        case id
        //case userAge = "user_age"
    }
}

