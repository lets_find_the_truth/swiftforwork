//
//  NetworkManager.swift
//  SwiftForWork
//
//  Created by Admin on 9/18/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

protocol CustomErrorNative: Error {
    var localizedTitle: String { get }
    var localizedDescription: String { get }
}

class NetworkErrorNative: CustomErrorNative {
    var localizedTitle: String
    var localizedDescription: String
    
    init(localizedTitle: String, localizedDescription: String) {
        self.localizedDescription = localizedDescription
        self.localizedTitle = localizedTitle
    }
}

class NetworkManagerNative {
    private lazy var session = URLSession(configuration: URLSessionConfiguration.default)
    
    func request<T: RequestHandlerNative>(model: RequestModelNative, handler: T.Type, completion: @escaping (T?, NetworkErrorNative?) -> Void) {
        
        var urlRequest = URLRequest(url: model.url)
        urlRequest.httpMethod = model.method.rawValue
        urlRequest.allHTTPHeaderFields = model.headers
        if let parameters = model.parameters {
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                completion(nil, NetworkErrorNative(localizedTitle: "Wrong params", localizedDescription: ""))
                return
            }
            urlRequest.httpBody = httpBody
        }

        session.dataTask(with: urlRequest) { (data, response, error) in
            guard error == nil else {
                completion(nil, NetworkErrorNative(localizedTitle: "Network error", localizedDescription: error!.localizedDescription))
                return
            }
            
            let response = response as! HTTPURLResponse
            if model.successCodes.contains(response.statusCode) {
                do {
                    let handler = try JSONDecoder().decode(handler, from: data!)
                    completion(handler, nil)
                } catch let error{
                    completion(nil, NetworkErrorNative(localizedTitle: "Decode error", localizedDescription: error.localizedDescription))
                }
            } else {
                completion(nil, NetworkErrorNative(localizedTitle: "Wrong status code", localizedDescription: "Status code is \(response.statusCode), expected status codes are \(model.successCodes)"))
            }
        }.resume()
    }
}

enum HTTPSessionMethod: String {
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}

protocol RequestModelNative {
    var url: URL { get set }
    var method: HTTPSessionMethod { get set }
    var headers: [String: String] { get set }
    var parameters: [String: Any]? { get set }
    var successCodes: [Int] { get set }
}



protocol RequestHandlerNative: Codable {
}
