//
//  UserDefaultsService.swift
//  SwiftForWork
//
//  Created by Admin on 3/14/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class UserDefaultsService {
    func saveObject(value: Any, key: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func getObject(key: String) -> Any? {
        return UserDefaults.standard.object(forKey: key)
    }
}
