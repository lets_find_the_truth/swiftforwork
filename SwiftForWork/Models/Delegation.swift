//
//  Delegation.swift
//  SwiftForWork
//
//  Created by Admin on 3/14/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

protocol BookDelegate {
    func printPage(text: String)
}

class Book {
    private var pages = ["One", "Two"]
    var delegate: BookDelegate?
    
    init(delegate: BookDelegate) {
        self.delegate = delegate
    }
    
    func printPages() {
        for page in pages {
            delegate?.printPage(text: page)
        }
    }
}

class Printer: BookDelegate {
    func printPage(text: String) {
        print(text)
    }
}


//usage - Book(delegate: Printer()).printPages()
