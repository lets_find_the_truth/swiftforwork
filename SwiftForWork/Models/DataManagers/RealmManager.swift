//
//  RealmManager.swift
//  SwiftForWork
//
//  Created by Admin on 9/20/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {
    private lazy var realm: Realm = {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            schemaVersion: 0,
            
            migrationBlock: { migration, oldSchemaVersion in
                
                //if oldSchama less than schemaVersion do migration
                if (oldSchemaVersion < 0) {
                    // Realm will automatically detect new properties and removed properties
                    
                    // But if you want to rename old properties so that old value remained, use:
                    //migration.renameProperty(onType: RealmClassThatNeedingChange.className(), from: "oldPropertyName", to: "newPropertyName")
                    
                    // If you want to use old properies value to create new one than use:
                    // don't forget to change oldObjectPropertyName and newObjectPropertyName to your propoerties' names
                    /* migration.enumerateObjects(ofType: RealmClassThatNeedingChange.className(), { (oldObject, newObject) in
                     let someOldObjetValue = oldObject!["oldObjectPropertyName"] as! String
                     newObject!["newObjectPropertyName"] = someOldObjetValue
                     }) */
                }
        })
        
        Realm.Configuration.defaultConfiguration = config

        return try! Realm()
    }()
    
    func addObject(object: Object) {
        try! realm.write {
            realm.add(object)
        }
    }
    
    func deleteObject(object: Object) {
        try! realm.write {
            realm.delete(object)
        }
    }
    
    func updateObject(dog: TestDog, name: String) {
        try! realm.write {
            dog.name = name
            //can use KVC for chose property in runtime dog.setValue(name, forKey: "name")
        }
    }
    
    func getObjects() -> [TestDog] {
        return Array(realm.objects(TestDog.self))
         // Query using a predicate string
         var tanDogs = realm.objects(TestDog.self).filter("color = 'tan' AND name BEGINSWITH 'B'")
         
         // Query using an NSPredicate
         let predicate = NSPredicate(format: "color = %@ AND name BEGINSWITH %@", "tan", "B")
         tanDogs = realm.objects(TestDog.self).filter(predicate)
        
        // Sort tan dogs with names starting with "B" by name
        let sortedDogs = realm.objects(TestDog.self).filter("color = 'tan' AND name BEGINSWITH 'B'").sorted(byKeyPath: "name")
    }
    
    func deleteAll() {
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func runExample() {
        // (1) Create a Dog object and then set its properties
        var myDog = TestDog()
        myDog.name = "Rex"
        // (2) Create a Dog object from a dictionary
        let myOtherDog = TestDog(value: ["name" : "Pluto"])
        // (3) Create a Dog object from an array
        let myThirdDog = TestDog(value: ["Fido"])
        
        // Creating a book with the same primary key as a previously saved book for update
        let testPerson = TestPerson()
        testPerson.id = 1
        
        // Updating book with id = 1 or crating new if id = 1 not exist
        try! realm.write {
            realm.add(testPerson, update: true)
        }
    }
}

//table view notifications
class ViewController: UITableViewController {
    var notificationToken: NotificationToken? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let realm = try! Realm()
        let results = realm.objects(TestPerson.self).filter("age > 5")
        
        // Observe Results Notifications
        notificationToken = results.observe { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                // Query results have changed, so apply them to the UITableView
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
            }
        }
    }
    
    deinit {
        notificationToken?.invalidate()
    }
}
//or only object
/*
var token : NotificationToken?
token = stepCounter.observe { change in
    switch change {
    case .change(let properties):
        for property in properties {
            if property.name == "steps" && property.newValue as! Int > 1000 {
                token = nil
            }
        }
    case .error(let error):
    case .deleted:
    }
}
*/
//also can obser all realm instanse

//MULTITHREADING
/*
The only thing you have to be aware of is that you cannot have multiple threads sharing the same instances of Realm objects. If multiple threads need to access the same objects they will each need to get their own instances (otherwise changes happening on one thread could cause other threads to see incomplete or inconsistent data).
 Instances of Realm, Results, or List, or managed instances of Object are thread-confined, meaning that they can only be used on the thread on which they were created, otherwise an exception is thrown*.
 */

//JSON
// A Realm Object that represents a city
class City: Object {
    @objc dynamic var city = ""
    @objc dynamic var id = 0
    // other properties left out ...
    func test() {
        let data = "{\"name\": \"San Francisco\", \"cityId\": 123}".data(using: .utf8)!
        let realm = try! Realm()
        
        // Insert from Data containing JSON
        try! realm.write {
            let json = try! JSONSerialization.jsonObject(with: data, options: [])
            realm.create(City.self, value: json, update: true)
        }
    }
}


 
//FOR EXAMPLE
class TestDog: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var owner: TestPerson? // Properties can be optional // to-one relationships must be optional
    //for all owners
    //let owners = LinkingObjects(fromType: Person.self, property: "dogs")
}
//GENERAL INFOMATION
/*
 Auto-updating results
 Results instances are live, auto-updating views into the underlying data, which means results never have to be re-fetched.
Object instances are live, auto-updating views into the underlying data; you never have to refresh objects. Modifying the properties of an object will immediately be reflected in any other instances referring to the same object.
 All changes to an object (addition, modification and deletion) must be done within a write transaction.
//DATA TYPES

Type    Non-optional                            Optional
Bool    @objc dynamic var value = false         let value = RealmOptional<Bool>()
Int     @objc dynamic var value = 0             let value = RealmOptional<Int>()
Float   @objc dynamic var value: Float = 0.0    let value = RealmOptional<Float>()
Double  @objc dynamic var value: Double = 0.0   let value = RealmOptional<Double>()
String  @objc dynamic var value = ""            @objc dynamic var value: String? = nil
Data    @objc dynamic var value = Data()        @objc dynamic var value: Data? = nil
Date    @objc dynamic var value = Date()        @objc dynamic var value: Date? = nil
Object  n/a: must be optional                   @objc dynamic var value: Class?
List    let value = List<Type>()                n/a: must be non-optional
LinkingObjects    let value = LinkingObjects(fromType: Class.self, property: "property")    n/a: must be non-optiona
 
 
 // FILTER
 The comparison operators ==, <=, <, >=, >, !=, and BETWEEN are supported for Int, Int8, Int16, Int32, Int64, Float, Double and Date property types, e.g. age == 45
 Identity comparisons ==, !=, e.g. Results<Employee>().filter("company == %@", company).
 The comparison operators == and != are supported for boolean properties.
 For String and Data properties, the ==, !=, BEGINSWITH, CONTAINS, and ENDSWITH operators are supported, e.g. name CONTAINS 'Ja'
 For String properties, the LIKE operator may be used to compare the left hand property with the right hand expression: ? and * are allowed as wildcard characters, where ? matches 1 character and * matches 0 or more characters. Example: value LIKE '?bc*' matching strings like “abcde” and “cbc”.
 Case-insensitive comparisons for strings, such as name CONTAINS[c] 'Ja'. Note that only characters “A-Z” and “a-z” will be ignored for case. The [c] modifier can be combined with the [d]` modifier.
 Diacritic-insensitive comparisons for strings, such as name BEGINSWITH[d] 'e' matching étoile. This modifier can be combined with the [c] modifier. (This modifier can only be applied to a subset of strings Realm supports: see limitations for details.)
 Realm supports the following compound operators: “AND”, “OR”, and “NOT”, e.g. name BEGINSWITH 'J' AND age >= 32.
 The containment operand IN, e.g. name IN {'Lisa', 'Spike', 'Hachi'}
 Nil comparisons ==, !=, e.g. Results<Company>().filter("ceo == nil"). Note that Realm treats nil as a special value rather than the absence of a value; unlike with SQL, nil equals itself.
 ANY comparisons, e.g. ANY student.age < 21.
 The aggregate expressions @count, @min, @max, @sum and @avg are supported on List and Results properties, e.g. realm.objects(Company.self).filter("employees.@count > 5") to find all companies with more than five employees.
 Subqueries are supported with the following limitations:
 @count is the only operator that may be applied to the SUBQUERY expression.
 The SUBQUERY(…).@count expression must be compared with a constant.
 Correlated subqueries are not yet supported.
 */

// Person model
class TestPerson: Object {
    @objc dynamic var name = ""
    @objc dynamic var birthdate = Date(timeIntervalSince1970: 1)
    @objc dynamic var id = 0
    let dogs = List<TestDog>()
    
    override static func primaryKey() -> String? {
        return "id"
    }//Declaring a primary key allows objects to be looked up and updated efficiently and enforces uniqueness for each value. Once an object with a primary key is added to a Realm, the primary key cannot be changed.
    
    override static func indexedProperties() -> [String] {
        return ["title"]
    }
    
    override static func ignoredProperties() -> [String] {
        return ["tmpID"]
    }//if you don't want to save this property
}


