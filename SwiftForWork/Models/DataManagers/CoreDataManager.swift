//
//  CoreDataManager.swift
//  SwiftForWork
//
//  Created by Admin on 9/17/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {
    static let shared = CoreDataManager()
    private init() {}
    
    lazy var persistenContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "SwiftForWork")
        container.loadPersistentStores(completionHandler: { (description, error) in
            guard error == nil else {
                return
            }
            
        })
        return container
    }()
    
    func saveContext() {
        let context = CoreDataManager.shared.persistenContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {}
        }
    }
    
    func insertPerson(name: String, age: Int) {
        let context = CoreDataManager.shared.persistenContainer.viewContext
        //let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: context)
        //        let person = NSManagedObject(entity: entityDescription!, insertInto: context)
        //        user.setValue(name, forKey: "name")
        //        user.setValue(age, forKey: "age")
        //OR
        //        let person = Person(entity: entityDescription!, insertInto: context)
        //OR
        let person = Person(context: context)
        person.age = Int64(age)
        person.name = name
        saveContext()
    }
    
    func updatePerson(name: String, age: Int, person: Person) {
        person.age = Int64(age)
        person.name = name
        saveContext()
    }
    
    func deleteObject(object: NSManagedObject) {
        let context = CoreDataManager.shared.persistenContainer.viewContext
        context.delete(object)
        saveContext()
    }
    
    func fetchAllPersons() -> [Person]? {
        let fetchRequest = NSFetchRequest<Person>(entityName: "Person")
        let context = CoreDataManager.shared.persistenContainer.viewContext
        //let ageSortDescriptor = NSSortDescriptor(key: "age", ascending: true)
        //        let nameSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        //        request.sortDescriptors = [nameSortDescriptor, ageSortDescriptor]
        //        request.predicate = NSPredicate(format: "name LIKE[cd] %@ AND age > %d", "*m", 3) //cd for any register case and diacritic insensitivity, can use CONTAINTS BEGINSWITH ENDSWITH LIKE(like containt but with more control * - 0 or more, ? - one character) MATCHES(regex syntax
        //fetchRequest.fetchLimit = 10
        //fetchRequest.fetchOffset = 10
        do {
            let results = try context.fetch(fetchRequest)
            return results
        } catch { return nil }
    }
    
}
